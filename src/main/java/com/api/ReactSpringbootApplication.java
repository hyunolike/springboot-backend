package com.api;

import com.api.model.User;
import com.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactSpringbootApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ReactSpringbootApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
		for(int i=1; i<=10; i++){
			this.userRepository.save(new User("Hyunho" + i, "Jang" + i, i + "test@test.com"));
		}

	}
}
