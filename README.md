# Spring Boot + React 
> Spring Boot, JPA, H2 Database

## 목차
1. [구현 방법](#1-구현-방법)
2. [실행 방법](#2-실행-방법)
3. [결과](#3-결과)

## 1 구현 방법
### 1-1. 환경 및 구조
- 기술
  - Spring Boot Web
  - Spring Data JPA
  - H2 Database
- 소프트웨어 프로그램
  - Java 11
  - Intellij IDEA
  
### 2 실행 방법
![](endpoint.jpg)

### 3 결과
> Json

![](result.jpg)
